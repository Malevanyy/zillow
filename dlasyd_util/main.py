import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os.path


def load_continuous_features():
    # noinspection PyUnresolvedReferences
    filename = os.path.join(os.path.dirname(__file__), '../continuous_features.txt')
    with open(filename) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    return content


def load_categorical_features():
    # noinspection PyUnresolvedReferences
    filename = os.path.join(os.path.dirname(__file__), '../categorical_features.txt')
    with open(filename) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    return content


def plot_learning_curve(train_scores, train_sizes, test_scores=None, log=False):
    plt.figure()
    plt.title("learning curve")
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    plt.grid()
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score",)
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="r")

    if test_scores is not None:
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)
        plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
        plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")

    if log:
        plt.yscale('symlog')

    plt.legend(loc="best")


def get_data_with_features(features):
    columns = list(features)
    columns.append('logerror')
    # noinspection PyUnresolvedReferences
    filename = os.path.join(os.path.dirname(__file__), '../raw_data/data_with_results.csv')
    data = pd.read_csv(filename)
    return data[columns]
